#!python
import os

WARNING_FLAGS = [
	'-Weverything',
	'-Werror=cast-qual',
	'-Werror=conversion',
	'-Werror=delete-incomplete',
	'-Werror=delete-non-virtual-dtor',
	'-Werror=deprecated',
	'-Werror=extra-semi',
	'-Werror=extra-tokens',
	'-Werror=float-conversion',
	'-Werror=implicit-fallthrough',
	'-Werror=inconsistent-missing-destructor-override',
	'-Werror=inconsistent-missing-override',
	'-Werror=invalid-pp-token',
	'-Werror=mismatched-new-delete',
	'-Werror=mismatched-tags',
	'-Werror=missing-declarations',
	'-Werror=missing-field-initializers',
	'-Werror=missing-prototypes',
	'-Werror=multichar',
	'-Werror=newline-eof',
	'-Werror=non-virtual-dtor',
	'-Werror=old-style-cast',
	'-Werror=return-type',
	'-Werror=self-move',
	'-Werror=shorten-64-to-32',
	'-Werror=sign-compare',
	'-Werror=sign-conversion',
	'-Werror=static-inline-explicit-instantiation', # note: GCC says that `inline template<>` is invalid syntax
	'-Werror=string-compare',
	'-Werror=strict-prototypes',
	'-Werror=string-compare',
	'-Werror=string-plus-int',
	'-Werror=uninitialized',
	'-Werror=unknown-pragmas',
	'-Werror=unknown-warning-option',
	'-Werror=unused-result',
	'-Werror=weak-vtables',
	'-Werror=zero-as-null-pointer-constant',
	'-Wno-c++98-compat',
	'-Wno-c++98-compat-pedantic',
	'-Wno-documentation-unknown-command',
	'-Wno-exit-time-destructors',
	'-Wno-float-equal',
	'-Wno-global-constructors',
	'-Wno-logical-op-parentheses',
	'-Wno-missing-braces',
	'-Wno-padded',
	'-Wno-shadow',
	'-Wno-undefined-func-template',
]

FSANITIZE = [
	'-fsanitize=address,undefined,nullability',
	'-fno-sanitize-recover=null',
]

CXXFLAGS = [
	'-std=c++17',
	'-isystem', 'lib',
	'-iquote', 'src',

	'-g',
	'-Og',

	'-fno-omit-frame-pointer',
	'-fstack-protector-strong',
	'-fvisibility=hidden',
]
CXXFLAGS += FSANITIZE
CXXFLAGS += WARNING_FLAGS

LINKFLAGS = [
	'-flto',
]
LINKFLAGS += FSANITIZE

LIBS = [
	'stdc++fs',
]

SRC = [
	'src/main.cpp',
	'src/matrix.cpp',
	'src/neural_nets.cpp',
]

env = Environment()
env['ENV']['TERM'] = os.environ['TERM']
env['CC'] = 'clang'
env['CXX'] = 'clang++'
env.Append(CXXFLAGS=CXXFLAGS)
env.Append(LINKFLAGS=LINKFLAGS)
env.Append(LIBS=LIBS)
env.ParseConfig('pkg-config --cflags --libs libpng')

target = env.Program(target='nn', source=SRC)
Default(target)
