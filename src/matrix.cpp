#include "matrix.hpp"

#include <algorithm>
#include <cassert>
#include <sstream>
#include <utility>

using std::size_t;
using std::string;

matrix::matrix(size_t rows, size_t cols)
:
	rows(rows),
	cols(cols),
	data(rows * cols)
{
}

matrix::matrix(matrix&& that)
:
	rows(that.rows),
	cols(that.cols),
	data(std::move(that.data))
{
}

matrix::matrix(const matrix& that)
:
	rows(that.rows),
	cols(that.cols),
	data(rows * cols)
{
	std::copy_n(that.data.cbegin(), rows * cols, data.begin());
}

matrix& matrix::operator=(matrix&& that)
{
	rows = that.rows;
	cols = that.cols;
	data = std::move(that.data);
	return *this;
}

matrix& matrix::operator=(const matrix& that)
{
	rows = that.rows;
	cols = that.cols;
	std::copy_n(that.data.cbegin(), rows * cols, data.begin());
	return *this;
}

matrix matrix::dot(const matrix& that) const
{
	assert(cols == that.rows);

	matrix m(rows, that.cols);
	for(size_t i = 0; i < rows; ++i)
	for(size_t j = 0; j < that.cols; ++j)
	{
		double sum = 0;
		for(size_t k = 0; k < cols; k++)
		{
			sum += at(i, k) * that.at(k, j);
		}
		m.at(i, j) = sum;
	}
	return m;
}

string matrix::to_string() const
{
	std::ostringstream ss;
	ss.precision(std::numeric_limits<double>::max_digits10);
	ss << "[ ";
	for(const double d : data)
	{
		ss << d << ' ';
	}
	ss << ']';
	return ss.str();
}

std::ostream& operator<<(std::ostream& o, const matrix& m)
{
	return o << m.to_string();
}
