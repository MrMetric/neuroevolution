#pragma once

#include <cstddef>
#include <functional>
#include <random>
#include <vector>

#include "matrix.hpp"

struct neural_nets
{
	std::size_t net_n;
	std::size_t input_n;
	std::size_t hidden_n;
	std::size_t output_n;
	std::vector<matrix> inputs;
	std::vector<matrix> hiddens;
	std::vector<matrix> outputs;

	neural_nets
	(
		std::size_t net_n,
		std::size_t input_n,
		std::size_t hidden_n,
		std::size_t output_n
	);

	/*
	 * in_data.size() == input_n
	 */
	std::vector<double> evaluate(const std::vector<double>& in_data, std::size_t net_i);

	/*
	 * in_data.size() == input_n
	 */
	std::vector<std::vector<double>> evaluate(const std::vector<double>& in_data);

	/*
	 * in_datas[?].size() == input_n
	 */
	std::vector<std::vector<std::vector<double>>> evaluate(const std::vector<std::vector<double>>& in_datas);

	/*
	 * errors.size() == net_n
	 */
	double evolve(const std::vector<double>& errors);

private:
	std::mt19937_64 engine;
	std::uniform_real_distribution<double> dist;
	std::uniform_int_distribution<std::size_t> dist2;
	std::function<double()> gen_rand;

	matrix crossover(const matrix&, const matrix&);
	void mutate(matrix&);
};
