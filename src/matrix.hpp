#pragma once

#include <cstddef>
#include <ostream>
#include <string>
#include <vector>

struct matrix
{
	std::size_t rows, cols; // height, width
	std::vector<double> data;

	matrix(std::size_t rows, std::size_t cols);

	matrix(matrix&&);
	matrix(const matrix&);
	matrix& operator=(matrix&&);
	matrix& operator=(const matrix&);

	// r = row = y
	// c = col = x
	double at(const std::size_t r, const std::size_t c) const
	{
		return data[c * rows + r];
	}
	double& at(const std::size_t r, const std::size_t c)
	{
		return data[c * rows + r];
	}

	matrix dot(const matrix&) const;

	std::string to_string() const;

	decltype(data)::iterator begin()
	{
		return data.begin();
	}
	decltype(data)::iterator end()
	{
		return data.end();
	}
};

std::ostream& operator<<(std::ostream& o, const matrix& m);
