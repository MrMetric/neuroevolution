#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <png++/png.hpp>

#include "neural_nets.hpp"

using std::cerr;
using std::cout;
using std::size_t;

int main(int argc, char** argv)
{
	if(argc != 1 + 1)
	{
		cerr << "syntax: " << argv[0] << " <population size>\n";
		return 1;
	}

	const size_t net_n = std::stoull(argv[1]);
	neural_nets nets(net_n, 2, 3, 1);

	std::vector<std::vector<double>> in_points;
	std::vector<double> in_types;

	std::ifstream in_file("moons");
	while(!in_file.eof())
	{
		double t, x, y;
		in_file >> t >> x >> y;
		in_points.emplace_back(std::vector<double>{x, y});
		in_types.emplace_back(t);
	}
	assert(in_points.size() == in_types.size());

	size_t generation;
	double lowest_error = 1;
	for(generation = 0; lowest_error > 0.02; ++generation)
	{
		const auto resultses = nets.evaluate(in_points);
		assert(resultses.size() == in_types.size());
		std::vector<double> errors(net_n);
		std::fill(errors.begin(), errors.end(), 0);
		for(size_t in_i = 0; in_i < resultses.size(); ++in_i)
		{
			const double expected_value = in_types[in_i];
			const auto& results = resultses[in_i];
			assert(results.size() == net_n);
			for(size_t net_i = 0; net_i < results.size(); ++net_i)
			{
				const double value = results[net_i][0];
				const double error = expected_value - value;
				errors[net_i] += std::abs(error);
			}
		}

		auto div_by_in_count = [n=in_points.size()](const double x) -> double
		{
			return x / n;
		};
		std::transform
		(
			errors.begin(), errors.end(),
			errors.begin(),
			div_by_in_count
		);

		/*
		cout << "generation " << generation << '\n';
		for(size_t net_i = 0; net_i < net_n; ++net_i)
		{
			cout << "\tnet " << net_i << '\n';
			cout << "\t\terror : " << errors[net_i] << '\n';
			cout << "\t\tinput : " << nets.inputs [net_i] << '\n';
			cout << "\t\thidden: " << nets.hiddens[net_i] << '\n';
			cout << "\t\toutput: " << nets.outputs[net_i] << '\n';
		}
		*/

		double new_lowest_error = nets.evolve(errors);
		if(new_lowest_error > lowest_error)
		{
			cout << "ERROR: new lowest error (" << new_lowest_error << ") > lowest error (" << lowest_error << ")\n";
			return 0;
		}
		lowest_error = new_lowest_error;
		//cout << "lowest error: " << lowest_error << '\n';
	}

	cout << "lowest error: " << lowest_error << '\n';
	cout << "generations : " << generation << '\n';

{
	std::ofstream out_file("moons_nn");
	for(size_t in_i = 0; in_i < in_points.size(); ++in_i)
	{
		const auto& point = in_points[in_i];
		const std::vector<double> result = nets.evaluate(point, 0);
		assert(result.size() == 1);
		const double expected = in_types[in_i];
		const double value = result[0];
		out_file << point[0] << ' ' << point[1]
		         << ' ' << expected << ' ' << std::round(value)
		         << ' ' << value
		         << ' ' << value - expected
		         << '\n';
	}
}

{
	std::vector<std::vector<double>> in_range;
	const double BOUND_X0 = -1.5;
	const double BOUND_X1 =  2.5;
	const double BOUND_Y0 = -1;
	const double BOUND_Y1 =  1.5;
	const double RANGE_X = BOUND_X1 - BOUND_X0;
	const double RANGE_Y = BOUND_Y1 - BOUND_Y0;
	const uint32_t WIDTH = 512;
	const uint32_t HEIGHT = static_cast<uint32_t>(std::ceil(WIDTH / RANGE_X * RANGE_Y));
	const double INTERVAL = RANGE_X / WIDTH;
	png::image<png::rgb_pixel> image(WIDTH, HEIGHT);
	for(uint32_t py = 0; py < HEIGHT; ++py)
	for(uint32_t px = 0; px < WIDTH; ++px)
	{
		const double dx = px * INTERVAL + BOUND_X0;
		const double dy = (HEIGHT - py) * INTERVAL + BOUND_Y0;
		const double value = nets.evaluate({dx, dy}, 0)[0];
		assert(value >= 0 && value <= 1);
		const uint8_t pixel = static_cast<uint8_t>(std::round(value * 255));
		image.set_pixel(px, py, png::rgb_pixel(pixel, pixel, pixel));
	}
	image.write("moons_nn.png");
}

	return 0;
}
