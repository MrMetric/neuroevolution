#include "neural_nets.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <numeric>
#include <utility>

using std::size_t;

// can not be constexpr because std::tanh is not constexpr
static double sigmoid(const double x)
{
	return (std::tanh(x / 2) + 1) / 2;
}

static void activate(matrix& m)
{
	std::transform
	(
		m.begin(), m.end(), // source
		m.begin(), // dest
		sigmoid
	);
}

neural_nets::neural_nets
(
	size_t net_n,
	size_t input_n,
	size_t hidden_n,
	size_t output_n
)
:
	net_n(net_n),
	input_n(input_n),
	hidden_n(hidden_n),
	output_n(output_n),
	// + 1 is for bias
	inputs(net_n, matrix(hidden_n, input_n + 1)),
	hiddens(net_n, matrix(hidden_n, hidden_n + 1)),
	outputs(net_n, matrix(output_n, hidden_n + 1)),
	engine(0), // arg is seed
	dist(-1, 1),
	dist2(0, net_n / 2),
	gen_rand([this]() { return dist(engine); })
{
	for(size_t i = 0; i < net_n; ++i)
	{
		auto& input = inputs[i];
		std::generate(input.begin(), input.end(), gen_rand);
	}
	for(size_t i = 0; i < net_n; ++i)
	{
		auto& hidden = hiddens[i];
		std::generate(hidden.begin(), hidden.end(), gen_rand);
	}
	for(size_t i = 0; i < net_n; ++i)
	{
		auto& output = outputs[i];
		std::generate(output.begin(), output.end(), gen_rand);
	}
}

static void add_bias(matrix& m)
{
	assert(m.cols == 1);
	++m.rows;
	m.data.emplace_back(1); // bias
}

std::vector<double> neural_nets::evaluate(const std::vector<double>& in_data, const size_t net_i)
{
	assert(in_data.size() == input_n);

	matrix mat_in(input_n + 1, 1);
	std::copy_n(in_data.cbegin(), input_n, mat_in.begin());
	mat_in.data[input_n] = 1; // bias

	matrix mat_input_out = inputs[net_i].dot(mat_in);
	activate(mat_input_out);
	add_bias(mat_input_out);

	matrix mat_hidden_out = hiddens[net_i].dot(mat_input_out);
	activate(mat_hidden_out);
	add_bias(mat_hidden_out);

	matrix mat_output_out = outputs[net_i].dot(mat_hidden_out);
	activate(mat_output_out);
	return mat_output_out.data;
}

/*
in data = list<number>, same for all NNs
NN output = list<number>
result = list<NN output>, one per NN
*/
std::vector<std::vector<double>> neural_nets::evaluate(const std::vector<double>& in_data)
{
	assert(in_data.size() == input_n);

	matrix mat_in(input_n + 1, 1);
	std::copy_n(in_data.cbegin(), input_n, mat_in.begin());
	mat_in.data[input_n] = 1; // bias

	std::vector<std::vector<double>> results(net_n);
	for(size_t net_i = 0; net_i < net_n; ++net_i)
	{
		matrix mat_input_out = inputs[net_i].dot(mat_in);
		activate(mat_input_out);
		add_bias(mat_input_out);

		matrix mat_hidden_out = hiddens[net_i].dot(mat_input_out);
		activate(mat_hidden_out);
		add_bias(mat_hidden_out);

		matrix mat_output_out = outputs[net_i].dot(mat_hidden_out);
		activate(mat_output_out);
		results[net_i] = mat_output_out.data;
	}
	return results;
}

/*
in data = list<number>, same for all NNs
in datas = list<in data>
NN output = list<number>
NN outputs = list<NN output>, one input
result = list<NN outputs>
*/
std::vector<std::vector<std::vector<double>>> neural_nets::evaluate(const std::vector<std::vector<double>>& in_datas)
{
	std::vector<std::vector<std::vector<double>>> resultses;
	for(const auto& in_data : in_datas)
	{
		resultses.emplace_back(evaluate(in_data));
	}
	return resultses;
}

double neural_nets::evolve(const std::vector<double>& errors)
{
	assert(errors.size() == net_n);

	double lowest_error;
{
	std::vector<size_t> i_map(net_n);
	std::iota(i_map.begin(), i_map.end(), 0);
	auto cmp = [&errors](const size_t lhs, const size_t rhs) -> bool
	{
		return errors[lhs] < errors[rhs];
	};
	std::sort(i_map.begin(), i_map.end(), cmp);

	lowest_error = errors[i_map[0]];

	// TODO: there is likely a way to do this in-place

	std::vector<matrix> old_inputs (net_n, matrix(0, 0));
	std::vector<matrix> old_hiddens(net_n, matrix(0, 0));
	std::vector<matrix> old_outputs(net_n, matrix(0, 0));

	std::swap(inputs , old_inputs );
	std::swap(hiddens, old_hiddens);
	std::swap(outputs, old_outputs);

	for(size_t i = 0; i < i_map.size(); ++i)
	{
		const size_t old_i = i_map[i];
		std::swap(inputs [i], old_inputs [old_i]);
		std::swap(hiddens[i], old_hiddens[old_i]);
		std::swap(outputs[i], old_outputs[old_i]);
	}
}

	for(size_t net_i = net_n / 2; net_i < net_n; ++net_i)
	{
		const size_t p1_i = dist2(engine);
		const size_t p2_i = dist2(engine);

		inputs [net_i] = crossover(inputs [p1_i], inputs [p2_i]);
		hiddens[net_i] = crossover(hiddens[p1_i], hiddens[p2_i]);
		outputs[net_i] = crossover(outputs[p1_i], outputs[p2_i]);

		mutate(inputs [net_i]);
		mutate(hiddens[net_i]);
		mutate(outputs[net_i]);
	}

	return lowest_error;
}

matrix neural_nets::crossover(const matrix& m1, const matrix& m2)
{
	assert(m1.rows == m2.rows);
	assert(m1.cols == m2.cols);

	matrix m(m1.rows, m1.cols);
	for(size_t i = 0; i < m1.data.size(); ++i)
	{
		m.data[i] = (dist(engine) > 0) ? m2.data[i] : m1.data[i];
	}
	return m;
}

void neural_nets::mutate(matrix& m)
{
	for(double& d : m)
	{
		if(dist(engine) > 0)
		{
			d += dist(engine);
		}
	}
}
